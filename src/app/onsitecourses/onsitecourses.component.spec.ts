import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsitecoursesComponent } from './onsitecourses.component';

describe('OnsitecoursesComponent', () => {
  let component: OnsitecoursesComponent;
  let fixture: ComponentFixture<OnsitecoursesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnsitecoursesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnsitecoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
